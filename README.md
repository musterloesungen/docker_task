## Docker-Übungsaufgabe: Multistage Build und Container Hardening

### Aufgabenstellung

Erstellen Sie ein Docker-Image für eine einfache Webanwendung unter Verwendung eines Multistage-Builds. Das Endprodukt soll Sicherheitsaspekte wie das Ausführen als Non-Root-Benutzer berücksichtigen.

### Voraussetzungen

- Grundkenntnisse in Docker und Dockerfile-Konfiguration.
- Eine vorinstallierte Docker-Umgebung.

### Ziele

1. **Multistage Build**: Implementierung eines Docker-Multistage-Builds zur Reduzierung der Image-Größe und zur Trennung von Build- und Runtime-Abhängigkeiten.
2. **Container Hardening**: Konfiguration des Containers zur Ausführung mit einem Non-Root-Benutzer, um die Sicherheit zu erhöhen.

### Schritte

1. **Vorbereitung des Projekts**:
   - Erstellen Sie ein einfaches Webanwendungsprojekt (z.B. in Node.js, Python Flask oder einer anderen bevorzugten Sprache).

2. **Erstellen des Dockerfiles**:
   - **Erster Stage (Build Stage)**:
     - Verwenden Sie ein offizielles Basisimage für den Build-Prozess (z.B. `node:alpine` für Node.js).
     - Fügen Sie den Quellcode und die Abhängigkeiten hinzu.
     - Führen Sie Build-Schritte aus (z.B. `npm install` für Node.js).
   - **Zweiter Stage (Runtime Stage)**:
     - Verwenden Sie ein schlankes Basisimage für den Runtime-Container (z.B. `alpine`).
     - Kopieren Sie nur die notwendigen Artefakte aus dem Build Stage in den Runtime Container.
     - Stellen Sie sicher, dass keine Build-Tools oder -Abhängigkeiten im Endimage enthalten sind.

3. **Implementierung von Container Hardening**:
   - Fügen Sie einen neuen Benutzer hinzu und wechseln Sie zu diesem vor dem Start der Anwendung:
     ```Dockerfile
     RUN adduser -D nonrootuser
     USER nonrootuser
     ```
   - Stellen Sie sicher, dass keine unnötigen Berechtigungen vergeben werden.

4. **Erstellen und Testen des Images**:
   - Bauen Sie das Image mit `docker build -t mein-webapp-image .`.
   - Starten Sie den Container und testen Sie seine Funktionalität.

### Ergebnis

Ein Docker-Image für eine Webanwendung, das sowohl effizient (durch den Einsatz von Multistage Builds) als auch sicherer (durch das Ausführen als Non-Root-Benutzer) ist.
