## Passende Node.js Webanwendung zur Aufgabe

### Projektstruktur

```plaintext
meine-webapp/
│
├── src/
│   └── app.js
│
└── package.json
```

### Datei: `package.json`

```json
{
  "name": "meine-webapp",
  "version": "1.0.0",
  "description": "Einfache Node.js Webanwendung",
  "main": "src/app.js",
  "scripts": {
    "start": "node src/app.js",
    "build": "echo 'Build-Skript (falls erforderlich)'"
  },
  "dependencies": {
    "express": "^4.17.1"
  }
}
```

### Datei: `src/app.js`

```javascript
const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.send('Hallo Welt von meiner Node.js Webanwendung!');
});

app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
```

### Anleitung

1. **Projektstruktur erstellen**:
   - Erstellen Sie die oben angegebene Projektstruktur in einem neuen Verzeichnis.

2. **Abhängigkeiten installieren**:
   - Führen Sie `npm install` im Wurzelverzeichnis des Projekts aus, um die benötigten Abhängigkeiten zu installieren.

3. **Server starten**:
   - Starten Sie die Anwendung mit `npm start`.
   - Die Anwendung ist nun unter `http://localhost:3000` erreichbar.

### Hinweise

- Diese Anwendung verwendet Express, ein populäres Framework für Node.js, um einen einfachen Webserver zu erstellen.
- Der Server antwortet auf Anfragen an die Wurzel-URL (`/`) mit einer einfachen Nachricht.
- Die Anwendung ist bewusst einfach gehalten, um die Integration in den Docker-Container zu erleichtern.
